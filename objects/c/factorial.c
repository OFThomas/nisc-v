/**
 * @author      : oli (oli@oli-desktop)
 * @file        : factorial
 * @created     : Saturday Apr 08, 2023 19:53:05 BST
 */

int main() 
{
    int n = 2;
    int result = 1;

    for(int i = 0; i < n; i++)
    {
        result = result * i;
    }

    return 0;
}
