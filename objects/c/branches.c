/**
 * @author      : oli (oli@s3rv3r)
 * @file        : branches
 * @created     : Saturday Apr 22, 2023 20:55:23 BST
 */

int main()
{
    volatile int * a = 1;
    *a = 0;
    volatile int * b = 4;

    if(*a == 0)
        *b = 1;
    else
        *b = 2;

    return 0;
}

