# This is just an example to get you started. You may wish to put all of your
# tests into a single file, or separate them into multiple `test1`, `test2`
# etc. files (better names are recommended, just make sure the name starts with
# the letter 't').
#
# To run these tests, simply execute `nimble test`.
import niscvpkg/virtualHardware
import unittest

suite "CPU":
  test "Mem store and load":

    let m1 = memory(4)
    echo m1

    var mem = memory(16)
    mem.store(0, -1, 4)
    echo mem

    echo mem.load(adrs = 0, numBytes = 1)
    echo mem.load(adrs = 0, numBytes = 2)
    echo mem.load(adrs = 0, numBytes = 4)

    echo mem.loadu(adrs = 0, numBytes = 1)
    echo mem.loadu(adrs = 0, numBytes = 2)
    echo mem.loadu(adrs = 0, numBytes = 4)

    for i in 0 ..< 5:
      mem.store(i.uint32, 1, 1)

    echo mem
    echo mem.load(0, 1)
    echo mem.load(0, 2)
    echo mem.load(0, 4)

    echo mem.loadu(0, 1)
    echo mem.loadu(0, 2)
    echo mem.loadu(0, 4)
    
    check(mem.load(0, 1) == 1)

