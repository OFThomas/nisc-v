import niscv
import unittest

suite "niscv decoding single instructions":
  test "decode 0x00000013":
    niscv.main("0x00000013")

suite "niscv loading asm files":
  test "objects/asm/jump.o":
    niscv.main("objects/asm/jump.o")

  test "objects/asm/minimal_add.o":
    niscv.main("objects/asm/minimal_add.o")

suite "niscv loading c files":

  test "objects/c/branches.o":
    niscv.main("objects/c/branches.o")

  test "objects/c/factorial.o":
    niscv.main("objects/c/factorial.o")

  test "objects/c/loop.o":
    niscv.main("objects/c/loop.o")

  test "objects/c/minimalAdd.o":
    niscv.main("objects/c/minimalAdd.o")

  test "objects/c/multiply.o":
    niscv.main("objects/c/multiply.o")
