/**
 * @class       : mathjax
 * @author      : oli (oli@oli-desktop)
 * @created     : Sunday Jan 15, 2023 17:27:26 GMT
 * @description : mathjax
 */

window.MathJax = {
  tex: {
    inlineMath: [["\\(", "\\)"]],
    displayMath: [["\\[", "\\]"]],
    processEscapes: true,
    processEnvironments: true
      tags: 'ams'
  },
  options: {
    ignoreHtmlClass: ".*|",
    processHtmlClass: "arithmatex"
  }
};

document$.subscribe(() => { 
  MathJax.typesetPromise()
})

