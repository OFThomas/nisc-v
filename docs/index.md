# Welcome to nisc-v, the nim RISC-V emulator and digital logic simulator 

[Source code here](https://gitlab.com/OFThomas/nisc-v).

## Motivation 


### Library features


## Install 

Clone the repo and run nimble install
```
git clone https://gitlab.com/OFThomas/nisc-v.git
cd nisc-v
nimble install 
```
You're ready to go, make sure to checkout the examples in `/examples`.

### Requirements 

### To run 
For a program called `prog`, 
```
nim c -d:danger prog
```


# Plan
See the [plan](plan.md).

# Progress
The virtual machine code [virtualmachine.nim](virtualMachine.md).

