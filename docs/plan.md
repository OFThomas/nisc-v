# References 

We are going to follow _Computer organization and design RISC-V Edition_
by Patternson and Hennesy.

# Why


# How 


# Processor simulator 

# Functional simulators
Like a virtual machine

## Virtual machine 
Action of instructions on registers 

Implement just a small subset of the instructions -- probably
even a subset of the RV32I base integer) set. Something like:
- addi (some arithmetic)
- andi (some logic)
- jal, beq (some control)
- load, store (memory operations)
Maybe target about 10 in total.

Need to emulate its registers -- there are x0 - x31 + a program counter
Need to emulate a memory -- maybe a flat address space with a small amount
of space, and where everything is both program and data storage.

In addition: want some way to step through programs, need some some
convention for the entry point (e.g. first instruction executed is at
memory location 0), need some way to view the state of all the registers.
Need some way to specify initial state of the memory. 


# Performance simulators *much complex*
    - measure machine cycles to execute program

Model hardware desc language (without having to write VHDL)

There are two systems:
1. The processor (implementing the instruction set) with pipelining, etc.
  This is for modelling instructions throughput, stalls, etc.
2. The memory system with cache/memory hierarchy etc. This is for modelling
  memory related performance, like cache misses.


Decide on number of cycles for each instruction, models PICs. 

Use basic instructions from VM implementation and try and model them.

Like VM, implement a small subset of instructions. Also, input is the
initial state of the memory, etc. Stepping through now consists of stepping
through clock cycles (i.e. pipeline stages), rather than stepping through 
instructions. E.g. on average, each instruction may take two clock cycles,
but several instructions are going on at once, and instructions may
interact. The internal state is not only the value of the registers, but
also the value of all the registers internal to each pipeline stage.

- memory
- CPU registers 32
- Instruction fetch registers (IF):
- Instruction decode registers (ID):
- Instruction execution (e.g. ALU) registers: 
- Memory-stage registers registers:
- Write-back corresponds to the 32 Registers.


## Microarchitecture
_The organization of the processor, including the major functional units,
their interconnection and control._


# pipeline description 
Patterson and Hennessey pg 529, fig 4.26

IF -> ID -> EX -> MEM -> WB

Instruction     Instruction       Execute       Memory      write         
fetch       ->    decode     ->   stage    ->    Access ->  back
input-output type modelling 


# Instruction decoding 


```
# ----------------------- Instruction decoding --------------------------

# there are 4 instruction formats R I S U all 32 bits (fig 2.2)
#
# 31         25 24    20 19     15 14    12 11       7 6        0
# ---------------------------------------------------------------
# |   func7   | |  rs2 | |  rs1  | |funct3| |   rd   | | opcode |  R TYPE
#
# |      imm[11:0]     | |  rs1  | |funct3| |   rd   | | opcode |  I TYPE
#
# | imm[11:5] | |  rs2 | |  rs1  | |funct3| |imm[4:0]| | opcode |  S TYPE
#
# |            imm[31:12]                 | |   rd   | | opcode |  U TYPE
#
# Intermediate values are always sign extended 
#
# Two more are the B J formats (fig 2.3 in risc manual)
#     31    30       25 24    20 19   15 14    12 11       8      7   6        0
# ------------------------------------------------------------------------------
# |imm[12]| |imm[10:5]| |  rs2 | | rs1 | |funct3| |imm[4:1]| |imm[11]|| opcode |  B

#     31    30         21    20     19          12 11   7 6        0
# ------------------------------------------------------------------
# |imm[20]| | imm[10:1] | |imm[11]| | imm[19:12] | | rd | | opcode |  J
#
```

