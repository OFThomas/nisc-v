# Nisc-V

This repo contains minimal code to emulate small parts of the RISC-V ISA.

[Documentation is here](https://ofthomas.gitlab.io/nisc-v).

# To install dependencies and build the emulator 
```
nimble install
```

# External requirements
```
sudo apt-get install libelf-dev
```

# Installing the riscv toolchain
```
git clone https://github.com/riscv/riscv-gnu-toolchain
cd riscv-gnu-toolchain
./configure --prefix=/opt/riscv --with-arch=rv32i --with-abi=ilp32
make linux
```

# To build and run the tests
```
nimble test
```

# You can test on a RV32I object file using 
```
niscv ./pathtofile.o
```

# Next steps

I think we've got all of the RV32I instructions 

1. Generate integer instruction subset 


# To test

Write a C program, compile it to RISC-V assembly, then compile the assembly to an object file. 

Test the c program using the riscv toolchain. Then run the object file through the emulator.


