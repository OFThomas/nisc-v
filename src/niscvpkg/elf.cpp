/**
 * \file elf.cpp
 * \brief Implementation of the Elf reader
 *
 *
 */

#include <err.h>
#include <fcntl.h>
#include <gelf.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sysexits.h>
#include <unistd.h>

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <memory>

#include <ios>

#include "elf.hpp"

void printHex(const std::vector<std::uint8_t> & hex, unsigned leading_spaces,
        unsigned blocks_per_line, unsigned block_size, std::ostream & os)
{
    // Save flags
    std::ios oldState(nullptr);
    oldState.copyfmt(os);

    std::size_t line_num = 0; // Current line
    std::size_t block_num = 0; // Current block
    std::size_t byte_num = 0; // Current byte

    while (byte_num < hex.size()) {

        // Check for start of line, print leading spaces
        if (block_num == 0 and (byte_num % block_size == 0)) {
            for (std::size_t n = 0; n < leading_spaces; n++) {
                os << " ";
            }
        }

        // Print the byte (two hexadecimal digits)
        os << std::hex << std::setfill('0') << std::setw(2)
            << static_cast<unsigned>(hex[byte_num++]);

        // Check for block boundary, print space
        if (byte_num % block_size == 0) {
            block_num++;
            os << " ";
            // Check for line boundary, print newline if necessary
            if (blocks_per_line != 0) {
                if (block_num % blocks_per_line == 0) {
                    block_num = 0; // Reset block  number
                    line_num++;
                    os << std::endl;
                } 
            } 
        }
    }

    // Print a newline at the end, if not already at a line end.
    // Otherwise, you get two new lines if there hex block finishes
    // exactly at the end of a line
    if (!(block_num == 0 and (byte_num % block_size == 0))) {
        os << std::endl;
    }

    // Restore flags
    os.copyfmt(oldState);

}

/**
 * \brief Elf section data
 *
 */
Section::Section(Elf * e, const GElf_Shdr & header, std::size_t shstrndx)
{
    // Use the offset into the string offset table to get
    // the section name. Use elf_strptr to convert the string
    // located at an index in the table to a char * for use
    // by the program.
    char * str;
    if ((str = elf_strptr(e, shstrndx, header.sh_name)) == NULL ) {
        errx(EX_SOFTWARE, "elf_strptr() failed: %s.", elf_errmsg(-1));
    } else {
        name = str;
    }

    // Store the size
    size = header.sh_size;

    // Store the type
    type = header.sh_type;
}

Section::~Section()
{}

std::string Section::getName() const
{
    return name;
}

std::size_t Section::getSize() const
{
    return size;
}

// Expect the index as an argument
void Section::print(std::size_t index) const
{
    std::cout << "Section " << index << ": " << name << std::endl;
}


/**
 * \brief String table section
 *
 */
SecStrtab::SecStrtab(Elf * e, const GElf_Shdr & header, std::size_t shstrndx,
        Elf_Scn * scn)
    : Section{e, header, shstrndx}
{

    char * name = NULL;

    // Section index
    std::size_t sec_index = elf_ndxscn(scn);

    std::size_t offset = 0;
    while (true) {
        name = elf_strptr(e, sec_index, offset);
        if (name == NULL) {
            // Everything in the table has been read
            break;
        }

        strings.push_back(name);
        // Advance offset by string length
        offset += strings.back().length() + 1;
    }


}

// Expect the index as an argument
void SecStrtab::print(std::size_t index) const 
{
    std::cout << std::endl
        << "Section " << index << ": "
        << getName() << " (string table)" << std::endl;
    std::cout << "  Index\t|  String" << std::endl;
    // Print the strings
    for (std::size_t n = 0; n < strings.size(); n++) {
        std::cout << "  " << n << "\t|  \"" << strings[n] << "\""
            << std::endl;
    }
}

/// Get string at index in string table (note: not at byte offset)
std::string SecStrtab::getString(std::size_t index) const
{
    return strings[index];
}


/**
 * \brief Progbits table section
 *
 */
SecProgbits::SecProgbits(Elf * e, const GElf_Shdr & header, std::size_t shstrndx,
        Elf_Scn * scn)
    : Section{e, header, shstrndx}
{

    Elf_Data * data = NULL;
    std::size_t n = 0;
    std::uint8_t * p = nullptr;

    // Make a call to elf_getdata to read section data. 
    while (n < header.sh_size && (data = elf_getdata(scn, data)) != NULL) {

        p = (std::uint8_t *)data->d_buf;
        // Store all the data in the current buffer
        while (p < (std::uint8_t *)data->d_buf + data->d_size) {
            sec_data.push_back(*p);
            p++;
        }
    }

}

// Expect the index as an argument
void SecProgbits::print(std::size_t index) const 
{
    std::cout << std::endl
        << "Section " << index << ": "
        << getName() << " (progbits section)" << std::endl;
    printHex(sec_data, 2, 4, 4);
}

/// Get the data in the section
std::vector<std::uint8_t> SecProgbits::getData() const
{
    return sec_data;
}


/** 
 * \brief Create the correct section object based on type
 *
 * A simple function to construct the correct type of section based
 * on the sh_type field of the section header.
 */
std::shared_ptr<Section> readSection(Elf * e, const GElf_Shdr & header,
        Elf_Scn * scn, std::size_t shstrndx)
{
    switch (header.sh_type) {
        case SHT_STRTAB:
            return std::make_shared<SecStrtab>(e, header, shstrndx, scn);
        case SHT_PROGBITS:
            return std::make_shared<SecProgbits>(e, header, shstrndx, scn);
        default:
            return std::make_shared<Section>(e, header, shstrndx);
    }

}

Elf::Elf(const std::string & filename)
{
    // std::string filename = "test.o";
    // Initialise the library
    if (elf_version(EV_CURRENT) == EV_NONE ) {
        errx ( EX_SOFTWARE , "ELF library initialization failed: %s " ,
                elf_errmsg(-1));
    }

    // Open the file
    if ((fd = open(filename.c_str(), O_RDONLY , 0)) < 0) {
        err(EX_NOINPUT, "open \"%s\" failed ", filename.c_str());
    }

    // Initialise the elf object
    if ((e = elf_begin(fd, ELF_C_READ, NULL)) == NULL ) {
        errx(EX_SOFTWARE, "elf_begin() failed: %s . ", elf_errmsg(-1));
    }

    // Check whether the file is an elf
    if (elf_kind(e) != ELF_K_ELF) {
        errx (EX_DATAERR , "\"%s\" is not an ELF object.", filename.c_str());
    }

    // Get the string table index
    if (elf_getshdrstrndx(e, &shstrndx) != 0) {
        errx(EX_SOFTWARE , "getshdrstrndx() failed: %s.", elf_errmsg(-1));
    }

    // Copy contents of the string table
    // Get the section descriptor associated with the string table
    if ((scn = elf_getscn(e, shstrndx)) == NULL ) {
        errx(EX_SOFTWARE , "getscn() failed: %s.", elf_errmsg(-1));
    }

    if (gelf_getshdr(scn, &shdr) != &shdr) {
        errx(EX_SOFTWARE, "getshdr(shstrndx) failed: %s.", elf_errmsg(-1));
    }

    data = NULL;
    n = 0;

    scn = NULL;

    // Loop over sections in the elf object
    while ((scn = elf_nextscn(e, scn)) != NULL) {

        // Get the section header
        if (gelf_getshdr(scn, &shdr) != &shdr ) {
            errx(EX_SOFTWARE , "getshdr() failed: %s.", elf_errmsg(-1));
        }

        // Section index
        std::size_t sec_index = elf_ndxscn(scn);

        // Store the section
        // sections.insert(std::pair<std::size_t, std::shared_ptr<Section>>(
        //             sec_index,
        //             std::move(readSection(e, shdr, scn, shstrndx)))
        //         );	
        sections[sec_index] = readSection(e, shdr, scn, shstrndx);
    }

}

Elf::~Elf()
{
    // Close the elf object handle
    elf_end(e);

    // Close the file
    close(fd);

}

std::size_t Elf::getNumSections() const
{
    return sections.size();
}

void Elf::printAllSections() const
{
    for (const auto & [index,sect] : sections) {
        sect->print(index);
    }
}

void Elf::printSection(std::size_t index) const
{
    try {
        sections.at(index)->print(index);
    } catch (const std::out_of_range & e) {
        std::cerr << "Section index " << index << " is out of range"
            << std::endl; 
    }
}


std::vector<std::uint8_t> Elf::getTextSection() const
{
    // Loop over all sections
    for (const auto & [index,sect] : sections) {
        if (sect->getName() == ".text") {
            return dynamic_cast<SecProgbits*>(sect.get())->getData();
        }
    }

    // If a text section wasn't found, throw an exception
    throw std::out_of_range("Could not find a .text section");

}
