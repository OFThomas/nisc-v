import cppstl
import std/strutils
import std/sequtils
from os import splitPath

# stringify for elf outputs 
proc `$`*(elfV: CppVector[uint8]): string =
  const num = 32
  var oneInstr: string
  for idx, thing in elfv:
    # To match the disassembler online, 
    # https://jborza.com/emulation/2021/04/18/riscv-disassembler.html
    # I have to reverse the 4 bytes in each 32bit instruction, like so
    oneInstr = thing.toHex & oneInstr
    # every 4 bytes (32bit) put a cheeky space in there 
    if (idx+1) mod 4 == 0:
      result &= oneInstr & " "
      oneInstr = ""
    # new line every 32 bytes (8 32bit instructions)
    if (idx+1) mod num == 0:
      result &= "\n"

proc `$`*(int32seq: seq[uint32]): string =
  result = int32seq.mapit(it.toHex).foldl(a & " " & b)

proc to32bitInts*(elfV: CppVector[uint8]): seq[uint32] =
  for i in countup(0, elfV.len.int - 1, 4):
    let vptr = elfV[i].unsafeAddr
    let iptr = cast[ptr uint32](vptr)
    result.add iptr[]

# this is mainly a reminder for Oli
when not defined(cpp): {.error: "Need to compile with c++" .}

# https://nim-lang.org/docs/backends.html
{.compile: "elf.cpp".}
{.passc: "-std=c++2a -I" & currentSourcePath().splitPath.head.}
{.passl: "-lelf".}

# {.push header: "<elf.hpp>".}
type Elf* {.importcpp: "Elf", byref, header: "elf.hpp".} = object

proc constructElf(filename: cstring): Elf {.constructor, importcpp: "Elf(@)", constructor, header: "elf.hpp".}
proc getTextSection(this: Elf): CppVector[uint8] {.noSideEffect, noInit, importcpp: "getTextSection", header: "elf.hpp".}

proc getTextSection*(objPath: string): seq[uint32] =
  let elfFile = constructElf(objPath)
  let vec = elfFile.getTextSection()
  result = vec.to32bitInts()

