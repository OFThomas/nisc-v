/**
 * \file elf.hpp
 * \brief Functions for loading Elf files and extracting sections
 *
 * Link with -lelf (sudo apt install libelf-dev)
 *
 * Usage
 *
 * Elf elf{"test.o"};
 * std::vector<std::uint8_t> data = elf.getTextSection();
 *
 */

#ifndef ELF_HPP
#define ELF_HPP

#include <err.h>
#include <fcntl.h>
#include <gelf.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sysexits.h>
#include <unistd.h>

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <memory>

void printHex(const std::vector<std::uint8_t> & hex, unsigned leading_spaces,
        unsigned blocks_per_line, unsigned block_size,
        std::ostream & os = std::cout);

class Elf;

class Section
{
    std::string name; ///< The section name (from the string table)
    std::uint64_t type; ///< Section type
    std::uint64_t flags; ///< Section flags
    std::uint64_t address; ///< Section address
    std::uint64_t offset; ///< Section offset
    std::uint64_t size; ///< Section data size (in bytes)
    std::uint64_t link; ///< Section link field
    std::uint64_t info; ///< Section info field
    std::uint64_t address_align; ///< Section alignment constraints
    std::uint64_t entry_size; ///< The section entry size    

    public:
    Section(Elf * e, const GElf_Shdr & header, std::size_t shstrndx);
    ~Section();

    std::string getName() const;

    std::size_t getSize() const;

    // Expect the index as an argument
    virtual void print(std::size_t index) const;
};

/**
 * \brief A class for reading elf files
 *
 */
class Elf
{
    int fd; ///< Elf file
    Elf * e; ///< Elf object
    Elf_Scn * scn; ///< Section pointer
    Elf_Data * data; ///< Elf data structure
    char * name, * p, pc[4*sizeof(char)];

    GElf_Shdr shdr;
    std::size_t n, sz;

    std::size_t shstrndx; ///< String table index

    // Section headers
    std::vector<GElf_Shdr> sect_heads;

    // Contents of the string table
    std::vector<std::string> string_tab;

    // Cached section data
    std::map<std::size_t, std::shared_ptr<Section>> sections;

    public:
    
    Elf() {};

    Elf(const std::string & filename);

    ~Elf();

    /// Return the number of sections
    std::size_t getNumSections() const;

    /// Print sections
    void printAllSections() const;

    void printSection(std::size_t index) const;

    /// Return the first .text section
    std::vector<std::uint8_t> getTextSection() const;
};

class SecStrtab : public Section
{
    std::vector<std::string> strings;

    public:

    SecStrtab(Elf * e, const GElf_Shdr & header, std::size_t shstrndx,
            Elf_Scn * scn);

    // Expect the index as an argument
    void print(std::size_t index) const override;

    /// Get string at index in string table (note: not at byte offset)
    std::string getString(std::size_t index) const;
};

/**
 * \brief Progbits table section
 *
 */
class SecProgbits : public Section
{
    std::vector<std::uint8_t> sec_data;

    public:

    SecProgbits(Elf * e, const GElf_Shdr & header, std::size_t shstrndx,
            Elf_Scn * scn);

    // Expect the index as an argument
    void print(std::size_t index) const override;

    /// Get the data in the section
    std::vector<std::uint8_t> getData() const;

};

std::shared_ptr<Section> readSection(Elf * e, const GElf_Shdr & header,
        Elf_Scn * scn, std::size_t shstrndx);


#endif
