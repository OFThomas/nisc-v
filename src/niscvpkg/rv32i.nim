import virtualHardware

import std/bitops
import std/strutils
import std/strformat
import std/colors

# This file contains all the implementation for the riscV 32bit integer
# instruction set - RV32I
#
# The file is arranged as follows 
# 1. The immediate values types and converters are defined for
# register/immediate instructions i.e. addi 
# 2. We define types for each of the different instruction formats, I./e.
# Itype and Rtype instruction where each nim type has members for each of
# the parts of the instruction
# 3. we define procs for each of the operations acting on our CPU type
# (defined in virtualHardware) 
# 4. For each of the instruction types (defined in 2.) we provide a decode
# and encode proc. Where decode takes a uint32 and returns the corresponding nim
# type instruction and encode takes a nim type instruction and returns a
# uint32.
# 5. There is the assemble proc which takes the assemble mnemonic
# instruction as a string and returns the nim type instruction
# corresponding to that.
# 6. there is the execute proc which takes a nim type instruction and calls
# the proc for that decoded instruction on a cpu object 
#
# Typical usage would be as follows 
# var cpu = initCpu(8) # give the cpu 8 bytes of memory
# let i: uint32 = "00508093".fromHex # addi ra,ra,5
# let nimInstr = i.decode()
# cpu.execute(nimInstr)

# there are 4 instruction formats R I S U all 32 bits (fig 2.2)
#
# 31         25 24    20 19     15 14    12 11       7 6        0
# ---------------------------------------------------------------
# |   func7   | |  rs2 | |  rs1  | |funct3| |   rd   | | opcode |  R TYPE
#
# |      imm[11:0]     | |  rs1  | |funct3| |   rd   | | opcode |  I TYPE
#
# | imm[11:5] | |  rs2 | |  rs1  | |funct3| |imm[4:0]| | opcode |  S TYPE
#
# |            imm[31:12]                 | |   rd   | | opcode |  U TYPE
#
# Intermediate values are always sign extended 
#
# Two more are the B J formats (fig 2.3 in risc manual)
#     31    30       25 24    20 19   15 14    12 11       8      7   6        0
# ------------------------------------------------------------------------------
# |imm[12]| |imm[10:5]| |  rs2 | | rs1 | |funct3| |imm[4:1]| |imm[11]|| opcode |  B

#     31    30         21    20     19          12 11   7 6        0
# ------------------------------------------------------------------
# |imm[20]| | imm[10:1] | |imm[11]| | imm[19:12] | | rd | | opcode |  J
#

# size instruction format types and the Invalid instruction type
type InstrType* = enum
  RType,
  IType,
  SType,
  BType,
  UType,
  JType,
  Invalid

# Register type instructions
type RInstr = object
  name*: string
  opcode*: uint32
  funct3*: uint32
  funct7*: uint32
  rd*: uint32
  rs1*, rs2*: uint32

# Immediate type instructions
type IInstr = object
  name*: string
  opcode*: uint32
  funct3*: uint32
  rd*: uint32
  rs1*: uint32
  imm*: uint32

# Store type instructions
type SInstr = object
  name*: string
  opcode*: uint32
  funct3*: uint32
  rs1*, rs2*: uint32
  imm*: uint32

# Branch type instructions
type BInstr = object
  name*: string
  opcode*: uint32
  funct3*: uint32
  rs1*, rs2*: uint32
  imm*: uint32

# Upper immediate type instructions
type UInstr = object
  name*: string
  opcode*: uint32
  rd*: uint32
  imm*: uint32

# Jump type instructions
type JInstr = object
  name*: string
  opcode*: uint32
  rd*: uint32
  imm*: uint32

# The instruction type which consists of exactly one 
# of the above types of instruction (or invalid)
type Instr* = object
  case kind*: InstrType
  of RType: Rval: RInstr
  of IType: Ival: IInstr
  of SType: Sval: SInstr
  of BType: Bval: BInstr
  of UType: Uval: UInstr
  of JType: Jval: JInstr
  of Invalid: discard

const nameCol = colLime
const rdCol = colCyan
const rsCol = colYellow
const immCol = colRed

# Short printing 
proc `$`*(instr: RInstr): string =
  let name = fmt"{instr.name}".setColor(nameCol)
  let rd = fmt"{instr.rd.registerMap}".setColor(rdCol)
  let rs1 = fmt"{instr.rs1.registerMap}".setColor(rsCol)
  let rs2 = fmt"{instr.rs2.registerMap}".setColor(rsCol)
  result = "{name}\t{rd}, {rs1}, {rs2}".fmt

proc `$`*(instr: IInstr): string =
  let name = fmt"{instr.name}".setColor(nameCol)
  let rd = fmt"{instr.rd.registerMap}".setColor(rdCol)
  let rs1 = fmt"{instr.rs1.registerMap}".setColor(rsCol)
  let imm = fmt"{instr.imm.toSigned}".setColor(immCol)
  result = "{name}\t{rd}, {rs1}, {imm}".fmt

proc `$`*(instr: SInstr): string =
  let name = fmt"{instr.name}".setColor(nameCol)
  let rs1 = fmt"{instr.rs1.registerMap}".setColor(rsCol)
  let rs2 = fmt"{instr.rs2.registerMap}".setColor(rsCol)
  let imm = fmt"{instr.imm.toSigned}".setColor(immCol)
  result = "{name}\t{rs1}, {rs2}, {imm}".fmt

proc `$`*(instr: BInstr): string =
  let name = fmt"{instr.name}".setColor(nameCol)
  let rs1 = fmt"{instr.rs1.registerMap}".setColor(rsCol)
  let rs2 = fmt"{instr.rs2.registerMap}".setColor(rsCol)
  let imm = fmt"{instr.imm.toSigned}".setColor(immCol)
  result = "{name}\t{rs1}, {rs2}, {imm}".fmt

proc `$`*(instr: UInstr): string =
  let name = fmt"{instr.name}".setColor(nameCol)
  let rd = fmt"{instr.rd.registerMap}".setColor(rsCol)
  let imm = fmt"{instr.imm.toSigned}".setColor(immCol)
  result = "{name}\t{rd}, {imm}".fmt

proc `$`*(instr: JInstr): string =
  let name = fmt"{instr.name}".setColor(nameCol)
  let rd = fmt"{instr.rd.registerMap}".setColor(rsCol)
  let imm = fmt"{instr.imm.toSigned}".setColor(immCol)
  result = "{name}\t{rd}, {imm}".fmt

proc `$`*(instr: Instr): string =
  case instr.kind
  of Invalid: result = fmt"Invalid"
  of RType: result = fmt"{instr.Rval}"
  of IType: result = fmt"{instr.Ival}"
  of SType: result = fmt"{instr.Sval}"
  of BType: result = fmt"{instr.Bval}"
  of UType: result = fmt"{instr.Uval}"
  of JType: result = fmt"{instr.Jval}"

proc getRegisterStates*(cpu: CPU, instr: RInstr): string =
  let rd = fmt"{instr.rd.registerMap}: {cpu.reg[instr.rd]}".setColor(rdCol)
  let rs1 = fmt"{instr.rs1.registerMap}: {cpu.reg[instr.rs1]}".setColor(rsCol)
  let rs2 = fmt"{instr.rs2.registerMap}: {cpu.reg[instr.rs2]}".setColor(rsCol)
  result = "{rd}, {rs1}, {rs2}".fmt

proc getRegisterStates*(cpu: CPU, instr: IInstr): string =
  let rd = fmt"{instr.rd.registerMap}: {cpu.reg[instr.rd]}".setColor(rdCol)
  let rs1 = fmt"{instr.rs1.registerMap}: {cpu.reg[instr.rs1]}".setColor(rsCol)
  let imm = fmt"{instr.imm.toSigned}".setColor(immCol)
  result = "{rd}, {rs1}, {imm}".fmt

proc getRegisterStates*(cpu: CPU, instr: SInstr): string =
  let rs1 = fmt"{instr.rs1.registerMap}: {cpu.reg[instr.rs1]}".setColor(rsCol)
  let rs2 = fmt"{instr.rs2.registerMap}: {cpu.reg[instr.rs2]}".setColor(rsCol)
  let imm = fmt"{instr.imm.toSigned}".setColor(immCol)
  result = "{rs1}, {rs2}, {imm}".fmt

proc getRegisterStates*(cpu: CPU, instr: BInstr): string =
  let rs1 = fmt"{instr.rs1.registerMap}: {cpu.reg[instr.rs1]}".setColor(rsCol)
  let rs2 = fmt"{instr.rs2.registerMap}: {cpu.reg[instr.rs2]}".setColor(rsCol)
  let imm = fmt"{instr.imm.toSigned}".setColor(immCol)
  result = "{rs1}, {rs2}, {imm}".fmt

proc getRegisterStates*(cpu: CPU, instr: UInstr): string =
  let rd = fmt"{instr.rd.registerMap}: {cpu.reg[instr.rd]}".setColor(rdCol)
  let imm = fmt"{instr.imm.toSigned}".setColor(immCol)
  result = "{rd}, {imm}".fmt

proc getRegisterStates*(cpu: CPU, instr: JInstr): string =
  let rd = fmt"{instr.rd.registerMap}: {cpu.reg[instr.rd]}".setColor(rdCol)
  let imm = fmt"{instr.imm.toSigned}".setColor(immCol)
  result = "{rd}, {imm}".fmt

proc getRegisterState*(cpu: CPU, instr: Instr): string =
  case instr.kind
  of Invalid: result = fmt"Invalid"
  of RType: result = fmt"{cpu.getRegisterStates(instr.Rval)}"
  of IType: result = fmt"{cpu.getRegisterStates(instr.Ival)}"
  of SType: result = fmt"{cpu.getRegisterStates(instr.Sval)}"
  of BType: result = fmt"{cpu.getRegisterStates(instr.Bval)}"
  of UType: result = fmt"{cpu.getRegisterStates(instr.Uval)}"
  of JType: result = fmt"{cpu.getRegisterStates(instr.Jval)}"

# detailed printing for debugging
const opColor = "#cdb4db".parseColor # purple
const funct3Color = "#2a9d8f".parseColor    # teal
const nameColor  = "#e9c46a".parseColor # yellow
const rdColor = "#f4a261".parseColor    # orange
const rsColor = "#f4a261".parseColor    # orange
const immColor = "#e76f51".parseColor   # red
const funct7Color = "#e76f51".parseColor # red

proc debugStr(instr: RInstr): string =
  let name = fmt"{instr.name}".setColor(nameColor)
  let opcode = fmt"opcode {instr.opcode.int64.toBin(7)}".setColor(opColor)
  let funct3 = fmt"funct3 {instr.funct3.int64.toBin(3)}".setColor(funct3Color)
  let funct7 = fmt"funct7 {instr.funct7.int64.toBin(7)}".setColor(funct7Color)
  let rd = fmt"rd {instr.rd.registerMap}".setColor(rdColor)
  let rs1 = fmt"rs1 {instr.rs1.registerMap}".setColor(rsColor)
  let rs2 = fmt"rs2 {instr.rs2.registerMap}".setColor(rsColor)
  result = fmt"{opcode}, {funct3}, {funct7}, {name} {rd}, {rs1}, {rs2}"

proc debugStr(instr: IInstr): string =
  let name = fmt"{instr.name}".setColor(nameColor)
  let opcode = fmt"opcode {instr.opcode.int64.toBin(7)}".setColor(opColor)
  let funct3 = fmt"funct3 {instr.funct3.int64.toBin(3)}".setColor(funct3Color)
  let rd = fmt"rd {instr.rd.registerMap}".setColor(rdColor)
  let rs1 = fmt"rs1 {instr.rs1.registerMap}".setColor(rsColor)
  let imm = fmt"imm {instr.imm.toSigned}".setColor(immColor)
  result = fmt"{opcode}, {funct3}, {name} {rd}, {rs1}, {imm}"

proc debugStr(instr: SInstr): string =
  let name = fmt"{instr.name}".setColor(nameColor)
  let opcode = fmt"opcode {instr.opcode.int64.toBin(7)}".setColor(opColor)
  let funct3 = fmt"funct3 {instr.funct3.int64.toBin(3)}".setColor(funct3Color)
  let rs1 = fmt"rs1 {instr.rs1.registerMap}".setColor(rsColor)
  let rs2 = fmt"rs2 {instr.rs2.registerMap}".setColor(rsColor)
  let imm = fmt"imm {instr.imm.toSigned}".setColor(immColor)
  result = fmt"{opcode}, {funct3}, {name} {rs1}, {rs2}, {imm}"

proc debugStr(instr: BInstr): string =
  let name = fmt"{instr.name}".setColor(nameColor)
  let opcode = fmt"opcode {instr.opcode.int64.toBin(7)}".setColor(opColor)
  let funct3 = fmt"funct3 {instr.funct3.int64.toBin(3)}".setColor(funct3Color)
  let rs1 = fmt"rs1 {instr.rs1.registerMap}".setColor(rsColor)
  let rs2 = fmt"rs2 {instr.rs2.registerMap}".setColor(rsColor)
  let imm = fmt"imm {instr.imm.toSigned}".setColor(immColor)
  result = fmt"{opcode}, {funct3}, {name} {rs1}, {rs2}, {imm}"

proc debugStr(instr: UInstr): string =
  let name = fmt"{instr.name}".setColor(nameColor)
  let opcode = fmt"opcode {instr.opcode.int64.toBin(7)}".setColor(opColor)
  let rd = fmt"rd {instr.rd.registerMap}".setColor(rsColor)
  let imm = fmt"imm {instr.imm.toSigned}".setColor(immColor)
  result = fmt"{opcode}, {name} {rd}, {imm}"

proc debugStr(instr: JInstr): string =
  let name = fmt"{instr.name}".setColor(nameColor)
  let opcode = fmt"opcode {instr.opcode.int64.toBin(7)}".setColor(opColor)
  let rd = fmt"rd {instr.rd.registerMap}".setColor(rsColor)
  let imm = fmt"imm {instr.imm.toSigned}".setColor(immColor)
  result = fmt"{opcode}, {name} {rd}, {imm}"

proc debugStr*(instr: Instr): string =
  case instr.kind
  of Invalid: result = fmt"Invalid"
  of RType: result = fmt"{instr.Rval.debugStr}"
  of IType: result = fmt"{instr.Ival.debugStr}"
  of SType: result = fmt"{instr.Sval.debugStr}"
  of BType: result = fmt"{instr.Bval.debugStr}"
  of UType: result = fmt"{instr.Uval.debugStr}"
  of JType: result = fmt"{instr.Jval.debugStr}"

# https://stackoverflow.com/a/17719010
proc signExtend*(instr: uint32, signBitPos: int): uint32 =
  let m = 1'u32 shl (signBitPos - 1)
  result = (instr xor m) - m

proc toOpcode*(instr: uint32): uint32 =
  result = instr.bitsliced(0 .. 6)

proc toBin*(val: uint32): string =
  val.BiggestInt.toBin(7)

# load upper immediate
# puts uimm into the top 20 bits of reg rd 
# fills the lowest 12 bits with zeros 
proc lui*(cpu: var CPU, rd: uint32, uimm: uint32) =
  cpu.reg[rd] = uimm shl 12

# add upper immediate to pc
proc auipc*(cpu: var CPU, rd: uint32, uimm: uint32) =
  let offset = uimm shl 12
  let adrs = cpu.pc + offset
  cpu.reg[rd] = adrs

# unconditional jump
# jump and link 
proc jal*(cpu: var CPU, rd: uint32, imm: uint32) =
  cpu.reg[rd] = cpu.pc + imm.signExtend

proc jalr*(cpu: var CPU, rd, rs1: uint32, imm: uint32) =
  var adrs = cpu.reg[rs1] + imm
  adrs.clearBit(0) # set lsb to zero
  cpu.reg[rd] = cpu.pc + adrs

# Control transfer instructions 
# conditional branches
proc beq*(cpu: var CPU, rs1, rs2: uint32, imm: uint32) =
  if cpu.reg[rs1] == cpu.reg[rs2]:
    cpu.pc += imm - 4

proc bne*(cpu: var CPU, rs1, rs2: uint32, imm: uint32) =
  if cpu.reg[rs1] != cpu.reg[rs2]:
    cpu.pc += imm - 4

proc blt*(cpu: var CPU, rs1, rs2: uint32, imm: uint32) =
  if cpu.reg[rs1].toSigned < cpu.reg[rs2].toSigned:
    cpu.pc += imm - 4

proc bltu*(cpu: var CPU, rs1, rs2: uint32, imm: uint32) =
  if cpu.reg[rs1].toUnsigned < cpu.reg[rs2].toUnsigned:
    cpu.pc += imm - 4

proc bge*(cpu: var CPU, rs1, rs2: uint32, imm: uint32) =
  if cpu.reg[rs1].toSigned >= cpu.reg[rs2].toSigned:
    cpu.pc += imm - 4

proc bgeu*(cpu: var CPU, rs1, rs2: uint32, imm: uint32) =
  if cpu.reg[rs1].toUnsigned >= cpu.reg[rs2].toUnsigned:
    cpu.pc += imm - 4

# store register to memory
# the effective address is adding rs1 to sign extended 12-bit offset
# rs1 is the base, imm12 is the offset 
# rs2 is stored in the memory 
# Same as stores but loads word, half, byte 

proc lb*(cpu: var CPU, rd, rs1: uint32, imm: uint32) = 
  let adrs = cpu.reg[rs1] + imm
  cpu.reg[rd] = cpu.memory.load(adrs, numBytes = 1)

proc lw*(cpu: var CPU, rd, rs1: uint32, imm: uint32) = 
  let adrs = cpu.reg[rs1] + imm
  cpu.reg[rd] = cpu.memory.load(adrs, numBytes = 4)

proc lh*(cpu: var CPU, rd, rs1: uint32, imm: uint32) = 
  let adrs = cpu.reg[rs1] + imm
  cpu.reg[rd] = cpu.memory.load(adrs, numBytes = 2)

proc lbu*(cpu: var CPU, rd, rs1: uint32, imm: uint32) =
  let adrs = cpu.reg[rs1] + imm
  cpu.reg[rd] = cpu.memory.loadu(adrs, numBytes = 1)

proc lhu*(cpu: var CPU, rd, rs1: uint32, imm: uint32) =
  let adrs = cpu.reg[rs1] + imm
  cpu.reg[rd] = cpu.memory.loadu(adrs, numBytes = 2)

proc sb*(cpu: var CPU, rs1, rs2: uint32, imm: uint32) =
  let adrs = cpu.reg[rs1] + imm
  cpu.memory.store(adrs, cpu.reg[rs2], numBytes = 1)

proc sh*(cpu: var CPU, rs1, rs2: uint32, imm: uint32) =
  let adrs = cpu.reg[rs1] + imm
  cpu.memory.store(adrs, cpu.reg[rs2], numBytes = 2)

proc sw*(cpu: var CPU, rs1, rs2: uint32, imm: uint32) = 
  let adrs = cpu.reg[rs1] + imm
  cpu.memory.store(adrs, cpu.reg[rs2], numBytes = 4)

# rd, rs1, rs2, imm

# sign extends imm (12bit) and adds to rs1 and store in rd
proc addi*(cpu: var CPU, rd, rs1: uint32, imm: uint32) = 
  cpu.reg[rd] = cpu.reg[rs1] + imm

# set less than intermediate 
# set reg rd == 1 if reg rs1 < imm when both treated 
# as signed numbers 
proc slti*(cpu: var CPU, rd, rs1: uint32, imm: uint32) = 
  if cpu.reg[rs1].toSigned < imm.toSigned:
    cpu.reg[rd] = 1
  else:
    cpu.reg[rd] = 0

# compares as unsigned numbers 
proc sltiu*(cpu: var CPU, rd, rs1: uint32, imm: uint32) =
  if cpu.reg[rs1] < imm:
    cpu.reg[rd] = 1
  else:
    cpu.reg[rd] = 0

# logical ops
proc andi*(cpu: var CPU, rd, rs1: uint32, imm: uint32) =
  cpu.reg[rd] = cpu.reg[rs1] and imm

proc ori*(cpu: var CPU, rd, rs1: uint32, imm: uint32) = 
  cpu.reg[rd] = cpu.reg[rs1] or imm
  
proc xori*(cpu: var CPU, rd, rs1: uint32, imm: uint32) =
  cpu.reg[rd] = cpu.reg[rs1] xor imm

proc slli*(cpu: var CPU, rd, rs1: uint32, imm: uint32) = 
  let shamt = imm.bitsliced(0 .. 4)
  cpu.reg[rd] = cpu.reg[rs1] shl shamt

proc srli*(cpu: var CPU, rd, rs1: uint32, imm: uint32) = 
  let shamt = imm.bitsliced(0 .. 4)
  cpu.reg[rd] = cpu.reg[rs1] shr shamt

proc srai*(cpu: var CPU, rd, rs1: uint32, imm: uint32) = 
  let shamt = imm.bitsliced(0 .. 4)
  let signed = cpu.reg[rs1].toSigned
  let ars = signed.ashr(shamt)
  cpu.reg[rd] = ars.toUnsigned

# Reg - Reg operations 
proc add*(cpu: var CPU, rd, rs1, rs2: uint32) =
  cpu.reg[rd] = cpu.reg[rs1] + cpu.reg[rs2]

proc sub*(cpu: var CPU, rd, rs1, rs2: uint32) =
  cpu.reg[rd] = cpu.reg[rs1] - cpu.reg[rs2]

proc `and`*(cpu: var CPU, rd, rs1, rs2: uint32) =
  cpu.reg[rd] = cpu.reg[rs1] and cpu.reg[rs2]

proc `or`*(cpu: var CPU, rd, rs1, rs2: uint32) =
  cpu.reg[rd] = cpu.reg[rs1] or cpu.reg[rs2]

proc `xor`*(cpu: var CPU, rd, rs1, rs2: uint32) =
  cpu.reg[rd] = cpu.reg[rs1] xor cpu.reg[rs2]

proc slt*(cpu: var CPU, rd, rs1, rs2: uint32) =
  if cpu.reg[rs1].toSigned < cpu.reg[rs2].toSigned:
    cpu.reg[rd] = 1
  else:
    cpu.reg[rd] = 0

proc sltu*(cpu: var CPU, rd, rs1, rs2: uint32) =
  if cpu.reg[rs1].toUnsigned < cpu.reg[rs2].toUnsigned:
    cpu.reg[rd] = 1
  else:
    cpu.reg[rd] = 0

# shifts val in reg rs1 bt amount held in the lower
# 5 bits of reg rs2 
proc sll*(cpu: var CPU, rd, rs1, rs2: uint32) =
  let shamt = cpu.reg[rs2].bitsliced(0 .. 4).int8
  cpu.reg[rd] = cpu.reg[rs1] shl shamt

proc srl*(cpu: var CPU, rd, rs1, rs2: uint32) =
  let shamt = cpu.reg[rs2].bitsliced(0 .. 4).int8
  cpu.reg[rd] = cpu.reg[rs1] shr shamt

# have to cast reg value to signed type for right arithmetic shift 
# then cast back to unsigned for writing to reg rd
proc sra*(cpu: var CPU, rd, rs1, rs2: uint32) =
  let shamt = cpu.reg[rs2].bitsliced(0 .. 4).int8
  cpu.reg[rd] = cpu.reg[rs1].toSigned.ashr(shamt).toUnsigned

# FENCE instructions?


# Call and Breakpoints
# ECALL
# EBREAK

proc mnemonic(instr: UInstr): string =
  result = case instr.opcode:
    of 0b0110111: "lui"
    of 0b0010111: "auipc"
    else: "unknown"

proc mnemonic(instr: JInstr): string =
  result = case instr.opcode:
    of 0b1101111: "jal"
    else: "unknown"

proc mnemonic(instr: BInstr): string =
  result = case instr.opcode:
    of 0b1100011:
      case instr.funct3:
        of 0b000: "beq"
        of 0b001: "bne"
        of 0b100: "blt"
        of 0b101: "bge"
        of 0b110: "bltu"
        of 0b111: "bgeu"
        else: "unknown"
    else: "unknown"

proc mnemonic(instr: IInstr): string =
  result = case instr.opcode:
    of 0b0000011:
      case instr.funct3:
        of 0b000: "lb"
        of 0b001: "lh"
        of 0b010: "lw"
        of 0b100: "lbu"
        of 0b101: "lhu"
        else: "unknown"
    of 0b0010011:
      case instr.funct3:
        of 0b000: "addi"
        of 0b010: "slti"
        of 0b011: "sltiu"
        of 0b100: "xori"
        of 0b110: "ori"
        of 0b111: "andi"
        of 0b001: "slli"
        of 0b101:
          case instr.imm shr 5:
            of 0b0000000: "srli"
            of 0b0100000: "srai"
            else: "unknown"
        else: "unknown"
    of 0b1100111:
      case instr.funct3:
        of 0b000: "jalr"
        else: "unknown"
    else: "unknown"

proc mnemonic(instr: SInstr): string =
  result = case instr.opcode:
    of 0b0100011:
      case instr.funct3:
        of 0b000: "sb"
        of 0b001: "sh"
        of 0b010: "sw"
        else: "unknown"
    else: "unknown"

proc mnemonic(instr: RInstr): string =
  result = case instr.opcode:
    of 0b0110011:
      case instr.funct3:
          of 0b000:
            case instr.funct7:
              of 0b0000000: "add"
              of 0b0100000: "sub"
              else: "unknown"
          of 0b001: "sll"
          of 0b010: "slt"
          of 0b011: "sltu"
          of 0b100: "xor"
          of 0b101:
            case instr.funct7:
              of 0b0000000: "srl"
              of 0b0100000: "sra"
              else: "unknown"
          of 0b110: "or"
          of 0b111: "and"
          else: "unknown"
    else: "unknown"

proc mnemonic*(instr: Instr): string =
  result = case instr.kind:
    of Invalid: "invalid"
    of RType: instr.Rval.mnemonic
    of IType: instr.Ival.mnemonic
    of SType: instr.Sval.mnemonic
    of BType: instr.Bval.mnemonic
    of UType: instr.Uval.mnemonic
    of JType: instr.Jval.mnemonic

proc RDecode*(instr: uint32, verbose: bool = false): Instr =
  result = Instr(kind: RType)
  result.Rval.opcode = instr.bitsliced(0 .. 6)
  result.Rval.rd = instr.bitsliced(7 .. 11)
  result.Rval.funct3 = instr.bitsliced(12 .. 14)
  result.Rval.rs1 = instr.bitsliced(15 .. 19)
  result.Rval.rs2 = instr.bitsliced(20 .. 24)
  result.Rval.funct7 = instr.bitsliced(25 .. 31)
  result.Rval.name = result.mnemonic
  if verbose:
    echo result.debugStr

proc encode*(instr: RInstr): uint32 =
  let op = instr.opcode
  let rd = instr.rd shl 7
  let funct3 = instr.funct3 shl 12
  let rs1 = instr.rs1 shl 15
  let rs2 = instr.rs2 shl 20
  let funct7 = instr.funct7 shl 25
  result = op or rd or funct3 or rs1 or rs2 or funct7

proc IDecode*(instr: uint32, verbose: bool = false): Instr =
  result = Instr(kind: IType)
  result.Ival.opcode = instr.bitsliced(0 .. 6)
  result.Ival.rd = instr.bitsliced(7 .. 11)
  result.Ival.funct3 = instr.bitsliced(12 .. 14)
  result.Ival.rs1 = instr.bitsliced(15 .. 19)
  result.Ival.imm = instr.bitsliced(20 .. 31).signExtend(12)
  result.Ival.name = result.mnemonic
  if verbose:
    echo result.debugStr

proc encode*(instr: IInstr): uint32 =
  let op = instr.opcode
  let rd = instr.rd shl 7
  let funct3 = instr.funct3 shl 12
  let rs1 = instr.rs1 shl 15
  let imm = instr.imm shl 20
  result = op or rd or funct3 or rs1 or imm

proc SDecode*(instr: uint32, verbose: bool = false): Instr =
  result = Instr(kind: SType)
  result.Sval.opcode = instr.bitsliced(0 .. 6)
  result.Sval.funct3 = instr.bitsliced(12 .. 14)
  result.Sval.rs1 = instr.bitsliced(15 .. 19)
  result.Sval.rs2 = instr.bitsliced(20 .. 24)

  let immlower5 = instr.bitsliced(7 .. 11)
  let immupper7 = instr.bitsliced(25 .. 31) shl 5
  result.Sval.imm = (immupper7 or immlower5).signExtend(12)
  result.Sval.name = result.mnemonic
  if verbose:
    echo result.debugStr

proc encode*(instr: SInstr): uint32 =
  let op = instr.opcode
  let funct3 = instr.funct3 shl 12
  let rs1 = instr.rs1 shl 15
  let rs2 = instr.rs2 shl 20
  let immlower = instr.imm.bitsliced(0 .. 4) shl 7
  let immupper = instr.imm.bitsliced(5 .. 11) shl 25
  result = op or funct3 or rs1 or rs2 or immlower or immupper

proc UDecode*(instr: uint32, verbose: bool = false): Instr =
  result = Instr(kind: UType)
  result.Uval.opcode = instr.bitsliced(0 .. 6)
  result.Uval.rd = instr.bitsliced(7 .. 11)
  result.Uval.imm = instr.bitsliced(12 .. 31) shl 12
  result.Uval.name = result.mnemonic
  if verbose:
    echo result.debugStr

proc encode*(instr: UInstr): uint32 =
  let op = instr.opcode
  let rd = instr.rd shl 7
  let imm = instr.imm
  result = op or rd or imm

proc BDecode*(instr: uint32, verbose: bool = false): Instr =
  result = Instr(kind: BType)
  result.Bval.opcode = instr.bitsliced(0 .. 6)
  result.Bval.funct3 = instr.bitsliced(12 .. 14)
  result.Bval.rs1 = instr.bitsliced(15 .. 19)
  result.Bval.rs2 = instr.bitsliced(20 .. 24)

  let bs7 = instr.bitsliced(7 .. 7)
  let bs8to11 = instr.bitsliced(8 .. 11)
  let bs25to30 = instr.bitsliced(25 .. 30)
  let bs31 = instr.bitsliced(31 .. 31)

  let imm11 = bs7 shl 11
  let imm1To4 = bs8to11 shl 1
  let imm5To10 = bs25to30 shl 5
  let imm12 = bs31 shl 12

  result.Bval.imm = (imm1To4 or imm5To10 or imm11 or imm12).signExtend(13)
  result.Bval.name = result.mnemonic
  if verbose:
    echo result.debugStr
    echo "Instr {instr}".fmt
    let bitstring = instr.int64.toBin(32)
    var outstr = ""
    for i in 0 ..< bitstring.len:
      if i mod 4 == 0 and i != 0:
        outstr.add " "
      outstr.add bitstring[i]

    echo "Instr to bits {outstr}".fmt
    echo "bitsliced(7 .. 7) \t {bs7}, imm11 \t {imm11}".fmt
    echo "bitsliced(8 .. 11) \t {bs8to11}, imm1To4 \t {imm1To4}".fmt
    echo "bitsliced(25 .. 30) \t {bs25to30}, imm5To10 \t {imm5To10}".fmt
    echo "bitsliced(31 .. 31) \t {bs31}, imm12 \t {imm12}".fmt
    echo "imm.signExtend \t {result.Bval.imm.signExtend}".fmt
    echo "imm.toSigned \t {result.Bval.imm.toSigned}".fmt

proc encode*(instr: BInstr): uint32 =
  let op = instr.opcode
  let funct3 = instr.funct3 shl 12
  let rs1 = instr.rs1 shl 15
  let rs2 = instr.rs2 shl 20

  let imm11 = instr.imm.bitsliced(11 .. 11) shl 7
  let imm4To1 = instr.imm.bitsliced(1 .. 4) shl 8
  let imm10To5 = instr.imm.bitsliced(5 .. 10) shl 25
  let imm12 = instr.imm.bitsliced(12 .. 12) shl 31

  result = op or funct3 or rs1 or rs2 or imm11 or imm4To1 or imm10To5 or imm12

proc JDecode*(instr: uint32, verbose: bool = false): Instr =
  result = Instr(kind: JType)
  result.Jval.opcode = instr.bitsliced(0 .. 6)
  result.Jval.rd = instr.bitsliced(7 .. 11)

  let imm12To19 = instr.bitsliced(12 .. 19) shl 12
  let imm11 = instr.bitsliced(20 .. 20) shl 11
  let imm1To10 = instr.bitsliced(21 .. 30) shl 1
  let imm20 = instr.bitsliced(31 .. 31) shl 20
  result.Jval.imm = (imm1To10 or imm11 or imm12To19 or imm20).signExtend(20)
  result.Jval.name = result.mnemonic
  if verbose:
    echo result.debugStr
    echo "imm1To10 {imm1To10} imm11 {imm11} imm12To19 {imm12To19} imm20 {imm20}".fmt

proc encode*(instr: JInstr): uint32 =
  let op = instr.opcode
  let rd = instr.rd

  let imm19To12 = instr.imm.bitsliced(12 .. 19) shl 12
  let imm11 = instr.imm.bitsliced(11 .. 11) shl 20
  let imm10To1 = instr.imm.bitsliced(1 .. 10) shl 21
  let imm20 = instr.imm.bitsliced(20 .. 20) shl 31

  result = op or rd or imm10To1 or imm11 or imm19To12 or imm20

proc decode*(instr: uint32, verbose: bool = false): Instr =
  let op = instr.toOpcode
  # echo "op ", op, " tobin ", op.to
  result = case op:
    of 0b0110111: instr.UDecode(verbose)
    of 0b0010111: instr.UDecode(verbose)

    of 0b1101111: instr.JDecode(verbose)
    of 0b1100111: instr.IDecode(verbose)

    of 0b1100011: instr.BDecode(verbose)

    of 0b0000011: instr.IDecode(verbose)

    of 0b0100011: instr.SDecode(verbose)

    of 0b0010011: instr.IDecode(verbose)

    of 0b0110011: instr.RDecode(verbose)

    else: 
      echo "Not a valid opcode "
      return Instr(kind: Invalid)

proc assemble*(instr: string): uint32 =
  echo "Instruction is ", instr
  result = case instr:

    # load type instrs
    of "lui":
      UInstr(opcode: 0b0110111, rd: 0, imm: 0).encode
    of "auipc":
      UInstr(opcode: 0b0010111, rd: 0, imm: 0).encode

    # jump type 
    of "jal":
      JInstr(opcode: 0b1101111, rd: 0, imm: 0).encode
    of "jalr":
      IInstr(opcode: 0b1100111, rd: 0, funct3: 0b000, imm: 0).encode
    
    # branches 
    of "beq":
      BInstr(opcode: 0b1100011, funct3: 0b000).encode
    of "bne":
      BInstr(opcode: 0b1100011, funct3: 0b001).encode
    of "blt":
      BInstr(opcode: 0b1100011, funct3: 0b100).encode
    of "bge":
      BInstr(opcode: 0b1100011, funct3: 0b101).encode
    of "bltu":
      BInstr(opcode: 0b1100011, funct3: 0b110).encode
    of "bgeu":
      BInstr(opcode: 0b1100011, funct3: 0b111).encode

    # loads
    of "lb":
      IInstr(opcode: 0b0000011, funct3: 0b000).encode
    of "lh":
      IInstr(opcode: 0b0000011, funct3: 0b001).encode
    of "lw":
      IInstr(opcode: 0b0000011, funct3: 0b010).encode
    of "lbu":
      IInstr(opcode: 0b0000011, funct3: 0b100).encode
    of "lhu":
      IInstr(opcode: 0b0000011, funct3: 0b101).encode

    # stores 
    of "sb":
      SInstr(opcode: 0b0100011, funct3: 0b000).encode
    of "sh":
      SInstr(opcode: 0b0100011, funct3: 0b001).encode
    of "sw":
      SInstr(opcode: 0b0100011, funct3: 0b010).encode

    # immediate ops 
    of "addi":
      IInstr(opcode: 0b0010011, rd: 2, funct3: 0b000, rs1: 1, imm: 1).encode
    of "slti":
      IInstr(opcode: 0b0010011, rd: 2, funct3: 0b010, rs1: 1, imm: 1).encode
    of "sltiu":
      IInstr(opcode: 0b0010011, rd: 2, funct3: 0b011, rs1: 1, imm: 1).encode
    of "xori":
      IInstr(opcode: 0b0010011, rd: 2, funct3: 0b100, rs1: 1, imm: 1).encode
    of "ori":
      IInstr(opcode: 0b0010011, rd: 2, funct3: 0b110, rs1: 1, imm: 1).encode
    of "andi":
      IInstr(opcode: 0b0010011, rd: 2, funct3: 0b111, rs1: 1, imm: 1).encode
    of "slli":
      IInstr(opcode: 0b0010011, rd: 2, funct3: 0b001, rs1: 1, imm: 1).encode
    of "srli":
      IInstr(opcode: 0b0010011, rd: 2, funct3: 0b101, rs1: 1, imm: 1).encode
    of "srai":
      IInstr(opcode: 0b0010011, rd: 2, funct3: 0b101, rs1: 1, imm: 1).encode

    # register - register ops
    of "add":
      RInstr(opcode: 0b0110011, funct3: 0b000, funct7: 0b0000000).encode
    of "sub":
      RInstr(opcode: 0b0110011, funct3: 0b000, funct7: 0b0100000).encode
    of "sll":
      RInstr(opcode: 0b0110011, funct3: 0b001, funct7: 0b0000000).encode
    of "slt":
      RInstr(opcode: 0b0110011, funct3: 0b010, funct7: 0b0000000).encode
    of "sltu":
      RInstr(opcode: 0b0110011, funct3: 0b011, funct7: 0b0000000).encode
    of "xor":
      RInstr(opcode: 0b0110011, funct3: 0b100, funct7: 0b0000000).encode
    of "srl":
      RInstr(opcode: 0b0110011, funct3: 0b101, funct7: 0b0000000).encode
    of "sra":
      RInstr(opcode: 0b0110011, funct3: 0b101, funct7: 0b0100000).encode
    of "or":
      RInstr(opcode: 0b0110011, funct3: 0b110, funct7: 0b0000000).encode
    of "and":
      RInstr(opcode: 0b0110011, funct3: 0b111, funct7: 0b0000000).encode

    else:
      0

proc execute*(cpu: var CPU, instruction: Instr) =
  case instruction.kind: 
  of Invalid:
    echo "Invalid instruction"
    quit(1)

  of IType:
    let instr = instruction.Ival
    case instr.opcode:
    of 0b1100111:
      cpu.jalr(rd=instr.rd, rs1=instr.rs1, imm=instr.imm)

    # loads 
    of 0b0000011:
      case instr.funct3:
      of 0b000: cpu.lb(rd=instr.rd, rs1=instr.rs1, imm=instr.imm)
      of 0b001: cpu.lh(rd=instr.rd, rs1=instr.rs1, imm=instr.imm)
      of 0b010: cpu.lw(rd=instr.rd, rs1=instr.rs1, imm=instr.imm)
      of 0b100: cpu.lbu(rd=instr.rd, rs1=instr.rs1, imm=instr.imm)
      of 0b101: cpu.lhu(rd=instr.rd, rs1=instr.rs1, imm=instr.imm)
      else: discard "0000011 funct3 case not covered"

    # immediate ops 
    of 0b0010011:
      case instr.funct3:
      of 0b000: cpu.addi(rd=instr.rd, rs1=instr.rs1, imm=instr.imm)
      of 0b010: cpu.slti(rd=instr.rd, rs1=instr.rs1, imm=instr.imm)
      of 0b011: cpu.sltiu(rd=instr.rd, rs1=instr.rs1, imm=instr.imm)
      of 0b100: cpu.xori(rd=instr.rd, rs1=instr.rs1, imm=instr.imm)
      of 0b110: cpu.ori(rd=instr.rd, rs1=instr.rs1, imm=instr.imm)
      of 0b111: cpu.andi(rd=instr.rd, rs1=instr.rs1, imm=instr.imm)
      of 0b001: cpu.slli(rd=instr.rd, rs1=instr.rs1, imm=instr.imm)
      of 0b101: 
        case instr.imm.testbit(30):
          of false: cpu.srli(rd=instr.rd, rs1=instr.rs1, imm=instr.imm)
          of true:  cpu.srai(rd=instr.rd, rs1=instr.rs1, imm=instr.imm)
      else: discard "0010011 funct3 case not covered"
    else: discard "Unknown opcode"

  of RType:
    # register - register ops
    let instr = instruction.Rval
    case instr.opcode:
    of 0b0110011:
      case instr.funct3:
      of 0b000: 
        case instr.funct7.testbit(5):
          of false: cpu.add(rd=instr.rd, rs1=instr.rs1, rs2=instr.rs2)
          of true: cpu.sub(rd=instr.rd, rs1=instr.rs1, rs2=instr.rs2)
      of 0b001: cpu.sll(rd=instr.rd, rs1=instr.rs1, rs2=instr.rs2)
      of 0b010: cpu.slt(rd=instr.rd, rs1=instr.rs1, rs2=instr.rs2)
      of 0b011: cpu.sltu(rd=instr.rd, rs1=instr.rs1, rs2=instr.rs2)
      of 0b100: cpu.xor(rd=instr.rd, rs1=instr.rs1, rs2=instr.rs2)
      of 0b101:
        case instr.funct7.testbit(5):
          of false: cpu.srl(rd=instr.rd, rs1=instr.rs1, rs2=instr.rs2)
          of true: cpu.sra(rd=instr.rd, rs1=instr.rs1, rs2=instr.rs2)
      of 0b110: cpu.or(rd=instr.rd, rs1=instr.rs1, rs2=instr.rs2)
      of 0b111: cpu.and(rd=instr.rd, rs1=instr.rs1, rs2=instr.rs2)
      else: discard "0110011 funct3 case not covered"
    else: discard "Unknown opcode"

  of UType:
    # load type instrs
    let instr = instruction.Uval
    case instr.opcode:
    of 0b0110111: cpu.lui(rd=instr.rd, uimm=instr.imm)
    of 0b0010111: cpu.auipc(rd=instr.rd, uimm=instr.imm)
    else: discard "Unknown opcode"

  of BType:
    # branches 
    let instr = instruction.Bval
    case instr.opcode:
    of 0b1100011:
      case instr.funct3:
      of 0b000: cpu.beq(rs1=instr.rs1, rs2=instr.rs2, imm=instr.imm)
      of 0b001: cpu.bne(rs1=instr.rs1, rs2=instr.rs2, imm=instr.imm)
      of 0b100: cpu.blt(rs1=instr.rs1, rs2=instr.rs2, imm=instr.imm)
      of 0b101: cpu.bge(rs1=instr.rs1, rs2=instr.rs2, imm=instr.imm)
      of 0b110: cpu.bltu(rs1=instr.rs1, rs2=instr.rs2, imm=instr.imm)
      of 0b111: cpu.bgeu(rs1=instr.rs1, rs2=instr.rs2, imm=instr.imm)
      else: discard "1100011 funct3 case not covered"
    else: discard "Unknown opcode"

  of JType:
    # jump type 
    let instr = instruction.Jval
    case instr.opcode:
    of 0b1101111: cpu.jal(rd=instr.rd, imm=instr.imm)
    else: discard "Unknown opcode"

  of SType:
    # stores 
    let instr = instruction.Sval
    case instr.opcode:
    of 0b0100011:
      case instr.funct3:
        of 0b000: cpu.sb(rs1=instr.rs1, rs2=instr.rs2, imm=instr.imm)
        of 0b001: cpu.sh(rs1=instr.rs1, rs2=instr.rs2, imm=instr.imm)
        of 0b010: cpu.sw(rs1=instr.rs1, rs2=instr.rs2, imm=instr.imm)
        else: discard "0100011 funct3 case not covered"
    else: discard "Unknown opcode"
