import std/bitops
import std/strformat
import std/terminal
import std/colors

import std/strutils
import std/enumutils
{.used.} # this mutes the imported and not used warning for rv32i.nim

# This file provides the CPU type which is used by instruction subsets i.e.
# rv32i.nim

# You can make a new cpu using
# var cpu = initCPU(8) # gives 8 bytes of memory to cpu
# the registers are accessed with
# cpu.reg[0..31] # a number between 0 and 31 as in the 32 bit risc-v spec
# there are 32 registers 
# and the program counter is 
# cpu.pc 

# pretty printing and colors for the terminal
proc setColor*(str: string, color: Color): string =
  result = fmt"{ansiForegroundColorCode(color)}{str}{ansiResetCode}"

# flat address space, addressable by 32 bit ints
# virtual address space 
# 32bit addressable but only some subset of the whole range
type Memory* = object
  data*: seq[uint8] 

# read access
proc `[]`*(mem: Memory, adrs: uint32): uint8 {.inline.} =
  result = mem.data[adrs]

# write access 
proc `[]=`*(mem: var Memory, adrs: uint32, value: uint8) {.inline.} =
  mem.data[adrs] = value

proc len*(mem: Memory): int {.inline.} =
  result = mem.data.len

proc `$`*(mem: Memory): string =
  const numBytesPerLine = 5 * 4
  # result = "Mem:\t"
  for i, value in mem.data:
    if i != 0:
      if i mod numBytesPerLine == 0:
        result.add "\n{i}:\t".fmt
      elif i mod 4 == 0:
        result.add "| ".setColor(colGray)

    if value == 0:
      result.add "00 "
    else:
      result.add "{value.toHex} ".fmt.setColor(colCoral)

# memory object ctor
# number of bytes 
proc memory*(size: Natural): Memory =
  result = Memory(data: newSeq[uint8](size))

# helper function for load/store
template getUnsignedt*(anyInt: typedesc[SomeInteger]): untyped =
  when anyInt.sizeof == 1: uint8
  elif anyInt.sizeof == 2: uint16
  elif anyInt.sizeof == 4: uint32
  elif anyInt.sizeof == 8: uint64
  else:
    {.error: "Can't getUnsignedt of anyint".}
    anyint

# helper function for load/store
template getSignedt*(anyInt: typedesc[SomeInteger]): untyped =
  when anyInt.sizeof == 1: int8
  elif anyInt.sizeof == 2: int16
  elif anyInt.sizeof == 4: int32
  elif anyInt.sizeof == 8: int64
  else:
    {.error: "Can't getSignedt of anyint".}
    anyint

# casts uint to signed version
proc toSigned*[T: SomeInteger](x: T): auto =
  cast[getSignedt(T)](x)

# casts int to unsigned version
proc toUnsigned*[T: SomeInteger](x: T): auto =
  cast[getUnsignedt(T)](x)

# https://stackoverflow.com/questions/23176386/can-sign-zero-extension-be-accomplished-just-by-casting
proc signExtend*[T: SomeInteger](x: T): uint32 {.inline.} =
  result = cast[uint32](x.toSigned())

proc zeroExtend*[T: SomeInteger](x: T): uint32 {.inline.} =
  result = cast[uint32](x.toUnsigned())

# loads a int32
proc load*(mem: Memory, adrs: uint32, numBytes: static[int]): uint32 =
  for i in 0 ..< numBytes:
    let m = mem[adrs + i.uint32].signExtend()
    let val = m shl (i * 8)
    # echo "i = ", i, " result = ", result, " val = ", val
    result = result or val

# loads a uint32 
proc loadu*(mem: Memory, adrs: uint32, numBytes: static[int]): uint32 =
  for i in 0 ..< numBytes:
    let m = mem[adrs + i.uint32].zeroExtend()
    let val = m shl (i * 8)
    result = result or val

proc store*[T: SomeInteger](mem: var Memory, adrs: uint32, val: T, numBytes: Natural) =
  let uval = val.toUnsigned()
  for i in 0 ..< numBytes:
    let startidx = i * 8
    let endidx = startidx + 8
    mem[adrs + i.uint32] = uval.bitsliced(startidx ..< endidx).uint8


# ----------------- CPU ------------------------
# registers
type regLabel* = enum
  zero, ra, sp, gp, tp, t0, t1, t2, s0, s1, 
  a0, a1, a2, a3, a4, a5, a6, a7, 
  s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, 
  t3, t4, t5, t6

converter registerLookup*(label: regLabel): uint32 =
  result = label.symbolRank.uint32

# pg 137
proc registerMap*(regNum: range[0..31]): string =
  result = regLabel(regNum).symbolName

type Registers* = object
  data*: array[regLabel, uint32]

# read register
proc `[]`*(registers: Registers, rs: uint32): uint32 =
  result = registers.data[rs.regLabel]

# write to register
proc `[]=`*(registers: var Registers, rd, value: uint32) =
  # reg 0 must always be ZERO!
  if rd == 0:
    return 
  registers.data[rd.regLabel] = value

proc init*(reg: var Registers, memorySize: Natural) =
  for i in 0'u32 ..< 32'u32:
    reg[i] = 0'u32

  reg[zero] = 0'u32 
  reg[sp] = memorySize.uint32 div 2

proc `$`*(reg: Registers): string =
  # you might want to split on tp as well
  const newlineRegs = ["s1", "a7", "s11"]
  # result = "Reg: [ "
  for i in 0 ..< 32:
    let label = i.registerMap
    let value = reg[i.uint32]

    let coloredLabel = label.setColor(colGreen)
    let coloredValue = 
      if value == 0: "0"
      else: ($value).setColor(colCoral)
    result.add "{coloredLabel}: {coloredValue},\t".fmt
    if label in newlineRegs:
      result.add "\n\t".fmt
  result = result[0 .. ^3] & " ]"

# What the instructions do 

type CPU* = object
  pc*: uint32
  reg*: Registers
  memory*: Memory
  programMemory*: Memory
  # the current result of the fetch instruction
  instruction*: uint32
  # total number of instructions executed
  instructionCounter*: int

proc initcpu*(memorySize: Natural): CPU =
  result.memory = memory(memorySize)
  # set the registers!
  result.reg.init(memorySize = memorySize) 

proc initProgramMemory*(cpu: var CPU, program: seq[uint32]) =
  cpu.programMemory = memory(4 * (program.len + 0))
  for i, instr in program:
    let memAdrs = (4 * i).uint32
    cpu.programMemory.store(memAdrs, instr, 4)

proc fetchInstruction*(cpu: var CPU): uint32 =
  cpu.instruction = cpu.programMemory.loadu(cpu.pc, 4)
  cpu.pc += 4
  cpu.instructionCounter += 1
  result = cpu.instruction

proc currentInstruction*(cpu: CPU): uint32 =
  result = cpu.instruction

proc isLastInstruction*(cpu: CPU): bool =
  result = cpu.pc >= cpu.programMemory.len.uint32

proc `$`*(cpu: CPU): string =
  result = "CPU: "
  result &= "program counter: {cpu.pc},\t".fmt
  result &= "Total instructions executed: {cpu.instructionCounter}\n".fmt
  result &= "Program:{cpu.programMemory}\n".fmt
  result &= "Reg: [ {cpu.reg}\n".fmt
  result &= "Memory: {cpu.memory}\n".fmt
