import niscvpkg/elf
import niscvpkg/virtualHardware
import niscvpkg/rv32i

import std/strutils

import strformat
import parseopt

proc showHelp(): string =
  """
  Usage: niscv [options] <path>
  Options:
    -d, --debug                 Enable debug mode
    -m:num, -m=num,             Set the memory size to <num> bytes
    --memory:num, --memory=num, Set the memory size to <num> bytes
    -h, --help                  Show this help
  Path: 
    niscv myobject.o            Path to an object file.
    niscv 0x00000013            Or instructions can be passed using 0x<hex>
  """

proc instructionDebug(key: string) =
  let key = key[2 .. ^1].toUpperASCII
  echo "decoding 0x{key}".fmt
  let integer = fromHex[uint32](key)
  echo "integer = {integer}".fmt
  let bitstring = fromHex[int64](key).toBin(32)
  var outstr = ""
  for i in 0 ..< bitstring.len:
    if i mod 4 == 0 and i != 0:
      outstr.add " "
    outstr.add bitstring[i]

  echo "Instr to bits {outstr}".fmt

  let decoded = decode(integer, verbose = true)
  discard decoded

proc instructionInfo(cpu: CPU, instruction: uint32): string =
  result = "{cpu.pc}:\t{instruction.toHex}, {instruction.decode}".fmt
  result.add "\t Values {cpu.getRegisterState(instruction.decode)}".fmt

proc B*(num: Natural): Natural = 
  result = num

proc KiB*(num: Natural): Natural = 
  result = 1024 * num

proc MiB*(num: Natural): Natural = 
  result = 1024 * num.KiB

proc GiB*(num: Natural): Natural =
  result = 1024 * num.MiB

proc main*(cliArgs: string = "") = 
  const INTRO = 
    """
__       __     __                             __           _   ___________ _______    __
| |     / /__  / /________  ____ ___  ___     / /_____     / | / /  _/ ___// ____/ |  / /
| | /| / / _ \/ / ___/ __ \/ __ `__ \/ _ \   / __/ __ \   /  |/ // / \__ \/ /    | | / /
| |/ |/ /  __/ / /__/ /_/ / / / / / /  __/  / /_/ /_/ /  / /|  // / ___/ / /___  | |/ /
|__/|__/\___/_/\___/\____/_/ /_/ /_/\___/   \__/\____/  /_/ |_/___//____/\____/  |___/
"""
  echo INTRO
  echo showHelp()

  var path: string
  var debug = false
  var memorySize = 128.B

  # Read cli args
  var args = initOptParser(cliArgs, shortNoVal = {'d', 'h'})
  for kind, key, val in args.getopt():
    case kind
    of cmdShortOption, cmdLongOption:
      if key == "d" or key == "debug":
        echo "Debug mode enabled"
        debug = true
      if key == "m" or key == "memory":
        memorySize = val.parseInt
        echo fmt"memory of {memorySize} enabled"
      if key == "h" or key == "help":
        echo showHelp()
    of cmdArgument:
      if key[0..1].toUpperASCII == "0X":
        instructionDebug(key)
        return
      else:
        path = key
    else:
      echo "Unknown option"

  # fail case
  if path.isEmptyOrWhitespace:
    echo "Please provide a path to an object file"
    quit(1)
  
  let instructions: seq[uint32] = path.getTextSection()
  var cpu = initcpu(memorySize)
  cpu.initProgramMemory(instructions)

  while not cpu.isLastInstruction:

    let instruction = cpu.fetchInstruction()

    echo cpu.instructionInfo(instruction)

    let decoded = instruction.decode()

    cpu.execute(decoded)

    if debug:
      echo cpu

  echo cpu

when isMainModule:
  main()

