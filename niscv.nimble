# Package

version       = "0.3.1"
author        = "Oliver Thomas, John Scott"
description   = "RISC-V emulator and digital logic simulator"
license       = "GPL-2.0-or-later"
srcDir        = "src"
installExt    = @["nim"]
bin           = @["niscv"]


# Dependencies

requires "cppstl >= 0.6.2"
requires "nim >= 1.6.10"
requires "drawIt >= 0.5.4"
